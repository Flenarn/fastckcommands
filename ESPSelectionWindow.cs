﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FastCKCommands
{
    public partial class ESPSelectionWindow : Form
    {
        //Buttons
        private Button btn_OK;
        private Button btn_selectESP;

        //Textboxes
        private TextBox txt_selectedESP;

        //Strings
        public String parameters { get; set; }

        //Labels
        private Label lbl_selectESP;

        public ESPSelectionWindow()
        {
            InitializeComponent();

            btn_OK = new Button();
            btn_selectESP = new Button();

            lbl_selectESP = new Label();

            txt_selectedESP = new TextBox();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(160, 40);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 1;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;
            //
            // btn_selectESP
            //
            btn_selectESP.BackColor = Color.FromArgb(84, 84, 84);
            btn_selectESP.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_selectESP.FlatStyle = FlatStyle.Flat;
            btn_selectESP.Location = new Point(350, 8);
            btn_selectESP.Name = "btn_selectESP";
            btn_selectESP.Size = new Size(35, 22);
            btn_selectESP.TabIndex = 2;
            btn_selectESP.Text = "...";
            btn_selectESP.UseVisualStyleBackColor = false;

            //
            // lbl_selectESP
            //
            lbl_selectESP.AutoSize = true;
            lbl_selectESP.ForeColor = Color.Gainsboro;
            lbl_selectESP.Location = new Point(3, 11);
            lbl_selectESP.Name = "lbl_selectESP";
            lbl_selectESP.Size = new Size(289, 13);
            lbl_selectESP.TabIndex = 3;
            lbl_selectESP.Text = "Select ESP to convert:";

            //
            // txt_selectedESP
            //
            txt_selectedESP.BackColor = Color.FromArgb(84, 84, 84);
            txt_selectedESP.BorderStyle = BorderStyle.FixedSingle;
            txt_selectedESP.ForeColor = Color.Gainsboro;
            txt_selectedESP.Location = new Point(145, 8);
            txt_selectedESP.Name = "txt_selectedESP";
            txt_selectedESP.Size = new Size(199, 23);
            txt_selectedESP.TabIndex = 2;
            txt_selectedESP.PlaceholderText = "Select ESP...";
            txt_selectedESP.ReadOnly = true;

            Controls.Add(lbl_selectESP);

            Controls.Add(btn_OK);
            Controls.Add(btn_selectESP);

            Controls.Add(txt_selectedESP);

            btn_selectESP.Click += Btn_selectESP_Click;
            btn_OK.Click += Btn_OK_Click;

            //BackColor, Font & size limitations
            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Size = new Size(409, 110);

            PerformLayout();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            btn_OK_Process();
        }

        private void btn_OK_Process()
        {
            if(txt_selectedESP.Text == "")
            {
                using (var messageBox = new MessageBox("No ESP selected!", "You must select a ESP first."))
                {
                    messageBox.ShowDialog();
                };
            }
            else
            {
                if (File.Exists(@"CreationKit.exe"))
                {
                    System.Diagnostics.Process.Start(@"CreationKit.exe", parameters  + ":" + txt_selectedESP.Text);
                    this.Close();
                }
                else
                {
                    using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                    messageBox.ShowDialog();

                }
            }
        }

        private void Btn_selectESP_Click(object sender, EventArgs e)
        {
            SelectESP();
        }

        private void SelectESP()
        {
            //File selection Properties
            FileDialog fileDialogESM = new OpenFileDialog
            {
                DefaultExt = ".esp",
                Filter = "Elder Scrolls Plugin |*.esp"
            };
            _ = fileDialogESM.ShowDialog(this);

            txt_selectedESP.Text = Path.GetFileName(fileDialogESM.FileName);
        }
    }
}
