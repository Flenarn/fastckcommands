﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FastCKCommands
{
    public partial class GenerateSingleLipWindow : Form
    {

        //Buttons
        private Button btn_selectWAV;
        private Button btn_OK;

        //Textboxes
        private TextBox txt_selectedWAV;
        private TextBox txt_WAVText;

        //Labes
        private Label lbl_wavToProcess;
        private Label lbl_wavText;

        //Booleans
        private bool wavSelected = false;

        //Strings
        public string parameters { get; set; }
        private String relativePath;

        public GenerateSingleLipWindow()
        {
            InitializeComponent();

            btn_selectWAV = new Button();
            btn_OK = new Button();

            txt_selectedWAV = new TextBox();
            txt_WAVText = new TextBox();

            lbl_wavToProcess = new Label();
            lbl_wavText = new Label();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(130, 70);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 1;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;
            //
            // btn_selectWAV
            //
            btn_selectWAV.BackColor = Color.FromArgb(84, 84, 84);
            btn_selectWAV.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_selectWAV.FlatStyle = FlatStyle.Flat;
            btn_selectWAV.Location = new Point(306, 33);
            btn_selectWAV.Name = "btn_selectWAV";
            btn_selectWAV.Size = new Size(35, 22);
            btn_selectWAV.TabIndex = 2;
            btn_selectWAV.Text = "...";
            btn_selectWAV.UseVisualStyleBackColor = false;

            //
            // txt_selectedWAV
            //
            txt_selectedWAV.BackColor = Color.FromArgb(84, 84, 84);
            txt_selectedWAV.BorderStyle = BorderStyle.FixedSingle;
            txt_selectedWAV.ForeColor = Color.Gainsboro;
            txt_selectedWAV.Location = new Point(100, 33);
            txt_selectedWAV.Name = "txt_selectedESM";
            txt_selectedWAV.Size = new Size(199, 23);
            txt_selectedWAV.TabIndex = 2;
            txt_selectedWAV.PlaceholderText = "Select WAV...";
            txt_selectedWAV.ReadOnly = true;
            //
            // txt_WAVText
            //
            txt_WAVText.BackColor = Color.FromArgb(84, 84, 84);
            txt_WAVText.BorderStyle = BorderStyle.FixedSingle;
            txt_WAVText.ForeColor = Color.Gainsboro;
            txt_WAVText.Location = new Point(100, 8);
            txt_WAVText.Name = "txt_WAVText";
            txt_WAVText.Size = new Size(199, 23);
            txt_WAVText.TabIndex = 2;
            txt_WAVText.PlaceholderText = "Enter WAV text...";

            //
            // lbl_wavToProcess
            //
            lbl_wavToProcess.AutoSize = true;
            lbl_wavToProcess.ForeColor = Color.Gainsboro;
            lbl_wavToProcess.Location = new Point(3, 36);
            lbl_wavToProcess.Name = "lbl_wavToProcess";
            lbl_wavToProcess.Size = new Size(289, 13);
            lbl_wavToProcess.TabIndex = 3;
            lbl_wavToProcess.Text = "WAV to process:";
            //
            // lbl_wavText
            //
            lbl_wavText.AutoSize = true;
            lbl_wavText.ForeColor = Color.Gainsboro;
            lbl_wavText.Location = new Point(3, 11);
            lbl_wavText.Name = "lbl_wavText";
            lbl_wavText.Size = new Size(289, 13);
            lbl_wavText.TabIndex = 3;
            lbl_wavText.Text = "WAV text:";

            Controls.Add(txt_selectedWAV);
            Controls.Add(txt_WAVText);

            Controls.Add(btn_selectWAV);
            Controls.Add(btn_OK);

            Controls.Add(lbl_wavToProcess);
            Controls.Add(lbl_wavText);

            //BackColor, Font & size limitations
            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Size = new Size(362, 160);

            btn_selectWAV.Click += btn_SelectWAV_Click;
            btn_OK.Click += btn_OK_Click;

            PerformLayout();

        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            btn_OK_Process();
        }

        private void btn_OK_Process()
        {
            if (txt_WAVText.Text == "")
            {
                using MessageBox messageBox = new MessageBox("No WAV text", relativePath);
                messageBox.ShowDialog();
            }
            else if (!wavSelected)
            {
                using MessageBox messageBox = new MessageBox("No WAV selected!", "You must select a WAV first.");
                messageBox.ShowDialog();
            }
            else
            {
                if (File.Exists(@"CreationKit32.exe"))
                {
                    System.Diagnostics.Process.Start(@"CreationKit32.exe", parameters + ":" + relativePath + " " + txt_WAVText.Text);
                    this.Close();
                }
                else
                {
                    using MessageBox messageBox = new MessageBox("Could not start CreationKit32.exe", "Check that the file exists!");
                    messageBox.ShowDialog();
                }
            }
        }
    

        //Calls file selection
        private void btn_SelectWAV_Click(object sender, EventArgs e)
        {
            SelectWAV();
        }

        private void SelectWAV()
        {
            //File selection Properties (Only allow for ESMs for now)
            FileDialog fileDialogWAV = new OpenFileDialog
            {
                DefaultExt = ".wav",
                Filter = "Waveform |*.wav"
            };
            _ = fileDialogWAV.ShowDialog(this);

            relativePath = Path.GetFullPath(fileDialogWAV.FileName).Substring(Path.GetFullPath(fileDialogWAV.FileName).IndexOf("Data"));

            txt_selectedWAV.Text = Path.GetFileName(fileDialogWAV.FileName);
            wavSelected = true;
        }
    }
}
