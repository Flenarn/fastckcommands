# FastCKCommands

FastCKCommands is a .NET Core application for dealing with all of the command line arguments of the Creation Kit. 

## Installation & Usage

Download either the self-contained or .NET Core reliant application from the Nexus page, place it into your root Fallout 4 directory and run from there.

FastCKCommands currently support the following command line arguments:

*  BuildCDX
*  CompressPSG
*  ConvertMaterials
*  ConvertToESL
*  DepersistRefs
*  DumpNeededFiles
*  ExportDialogue
*  ExportDismemberData
*  ExportFaceGenData
*  ExportText
*  GenerateAnimInfo
*  GenerateLips
*  GeneratePreCombined
*  GeneratePreVisData
*  GenerateSingleLip
*  GenerateStaticCollections
*  GenerateWarnings
*  MapInfo
*  MapMaker
*  SaveDefaultPlugin
*  UpdateModelData

## Credits

[Nukem](https://github.com/Nukem9)
