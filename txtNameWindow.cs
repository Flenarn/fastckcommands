﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FastCKCommands
{
    public partial class txtNameWindow : Form
    {
        //Buttons
        private Button btn_OK;

        //Labels
        private Label lbl_outputFileName;

        //Textboxes
        private TextBox txt_outputFileName;

        //Strings
        public String parameters { get; set; }

        public txtNameWindow()
        {
            InitializeComponent();

            btn_OK = new Button();

            lbl_outputFileName = new Label();

            txt_outputFileName = new TextBox();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(130, 70);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 1;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            //
            // txt_outputFileName
            //
            txt_outputFileName.BackColor = Color.FromArgb(84, 84, 84);
            txt_outputFileName.BorderStyle = BorderStyle.FixedSingle;
            txt_outputFileName.ForeColor = Color.Gainsboro;
            txt_outputFileName.Location = new Point(115, 8);
            txt_outputFileName.Name = "txt_pluginName";
            txt_outputFileName.Size = new Size(199, 23);
            txt_outputFileName.TabIndex = 2;
            txt_outputFileName.PlaceholderText = "Enter a plugin name...";

            //
            // lbl_outputFileName
            //
            lbl_outputFileName.AutoSize = true;
            lbl_outputFileName.ForeColor = Color.Gainsboro;
            lbl_outputFileName.Location = new Point(3, 11);
            lbl_outputFileName.Name = "lbl_outputFileName";
            lbl_outputFileName.Size = new Size(289, 13);
            lbl_outputFileName.TabIndex = 3;
            lbl_outputFileName.Text = "Output file name:";

            Controls.Add(lbl_outputFileName);

            Controls.Add(btn_OK);

            Controls.Add(txt_outputFileName);

            //BackColor, Font & size limitations
            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Size = new Size(362, 160);

            btn_OK.Click += btn_OK_Click;

            PerformLayout();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            btn_OK_Process();
        }

        private void btn_OK_Process()
        {
            if (txt_outputFileName.Text == "")
            {
                using (var messageBox = new MessageBox("No plugin name!", "You must set a plugin name."))
                {
                    messageBox.ShowDialog();
                };
            }
            else
            {
                String tempName = txt_outputFileName.Text.ToLower();
                if(tempName.EndsWith(".txt"))
                {
                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + " " + txt_outputFileName.Text);
                        this.Close();
                    }
                    else
                    {
                        using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                    
                }
                else 
                {
                    txt_outputFileName.Text = txt_outputFileName.Text + ".txt";

                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + " " + txt_outputFileName.Text);
                        this.Close();
                    }
                    else
                    {
                        using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                }
            }
        }
    }
}
