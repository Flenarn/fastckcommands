﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FastCKCommands
{
    public partial class PlatformWindow : Form
    {
        //Checkboxes XB1|X64|PS4|W32 (RadioButtons)
        private RadioButton chk_XB1;
        private RadioButton chk_X64;
        private RadioButton chk_PS4;
        private RadioButton chk_W32;

        //Buttons
        private Button btn_OK;

        //Labels
        private Label lbl_selectPlatform;

        //Strings
        public string parameters { get; set; }
        private string parameterAddition;

        private void RadioButtonCheckedChanged(object sender, EventArgs e)
        {
            parameterAddition = "";
            if(chk_XB1.Checked)
            {
                parameterAddition = " XB1";
            }
            if(chk_X64.Checked)
            {
                parameterAddition = " X64";
            }
            if(chk_PS4.Checked)
            {
                parameterAddition = " PS4";
            }
            if(chk_W32.Checked)
            {
                parameterAddition = " W32";
            }
        }

        public PlatformWindow()
        {
            InitializeComponent();

            chk_XB1 = new RadioButton();
            chk_X64 = new RadioButton();
            chk_PS4 = new RadioButton();
            chk_W32 = new RadioButton();

            btn_OK = new Button();

            lbl_selectPlatform = new Label();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(120, 70);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 2;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            //
            // chk_XB1
            //
            chk_XB1.AutoSize = true;
            chk_XB1.FlatStyle = FlatStyle.Flat;
            chk_XB1.ForeColor = Color.Gainsboro;
            chk_XB1.Location = new Point(12, 41);
            chk_XB1.Name = "chk_XB1";
            chk_XB1.Size = new Size(185, 17);
            chk_XB1.TabIndex = 0;
            chk_XB1.Text = "XB1";
            chk_XB1.UseVisualStyleBackColor = true;
            //
            // chk_X64
            //
            chk_X64.AutoSize = true;
            chk_X64.FlatStyle = FlatStyle.Flat;
            chk_X64.ForeColor = Color.Gainsboro;
            chk_X64.Location = new Point(12, 66);
            chk_X64.Name = "chk_X64";
            chk_X64.Size = new Size(185, 17);
            chk_X64.TabIndex = 0;
            chk_X64.Text = "X64";
            chk_X64.UseVisualStyleBackColor = true;
            //
            // chk_PS4
            //
            chk_PS4.AutoSize = true;
            chk_PS4.FlatStyle = FlatStyle.Flat;
            chk_PS4.ForeColor = Color.Gainsboro;
            chk_PS4.Location = new Point(12, 91);
            chk_PS4.Name = "chk_PS4";
            chk_PS4.Size = new Size(185, 17);
            chk_PS4.TabIndex = 0;
            chk_PS4.Text = "PS4";
            chk_PS4.UseVisualStyleBackColor = true;
            //
            // chk_W32
            //
            chk_W32.AutoSize = true;
            chk_W32.FlatStyle = FlatStyle.Flat;
            chk_W32.ForeColor = Color.Gainsboro;
            chk_W32.Location = new Point(12, 116);
            chk_W32.Name = "chk_W32";
            chk_W32.Size = new Size(185, 17);
            chk_W32.TabIndex = 0;
            chk_W32.Text = "W32";
            chk_W32.UseVisualStyleBackColor = true;

            //
            // lbl_selectPlatform
            //
            lbl_selectPlatform.AutoSize = true;
            lbl_selectPlatform.ForeColor = Color.Gainsboro;
            lbl_selectPlatform.Location = new Point(3, 11);
            lbl_selectPlatform.Name = "lbl_selectPlatform";
            lbl_selectPlatform.Size = new Size(289, 13);
            lbl_selectPlatform.TabIndex = 3;
            lbl_selectPlatform.Text = "Select target platform for export:";

            Controls.Add(chk_XB1);
            Controls.Add(chk_X64);
            Controls.Add(chk_PS4);
            Controls.Add(chk_W32);
            Controls.Add(btn_OK);

            Controls.Add(lbl_selectPlatform);

            btn_OK.Click += btn_OK_Click;

            chk_XB1.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_X64.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_PS4.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_W32.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);

            //Sets chk_XB1 to be checked by default as it is the first option chronologically.
            chk_XB1.Checked = true;

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            Text = "Platform Selection";
            Size = new Size(280, 180);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            ForeColor = Color.Gainsboro;
            MaximizeBox = false;
            MinimizeBox = false;

            PerformLayout();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            StartProcess();
        }

        private void StartProcess()
        {
            if (File.Exists(@"CreationKit.exe"))
            {
                System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + parameterAddition);
                this.Close();
            }
            else
            {
                using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                messageBox.ShowDialog();
            }
        }
    }
}
