﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FastCKCommands
{
    public partial class MapInfoWindow : Form
    {
        //Buttons
        private Button btn_OK;

        //Checkboxes
        private RadioButton chk_doors;
        private RadioButton chk_markers;
        private RadioButton chk_interiors;

        //Strings
        public string parameters { get; set; }
        private string parameterAddition;

        //Labels
        private Label lbl_selectExportOption;

        private void RadioButtonCheckedChanged(object sender, EventArgs e)
        {
            parameterAddition = "";
            if (chk_doors.Checked)
            {
                parameterAddition = " doors";
            }
            if (chk_markers.Checked)
            {
                parameterAddition = " markers";
            }
            if (chk_interiors.Checked)
            {
                parameterAddition = " interiors";
            }

        }

        public MapInfoWindow()
        {
            InitializeComponent();
            lbl_selectExportOption = new Label();

            btn_OK = new Button();

            chk_doors = new RadioButton();
            chk_markers = new RadioButton();
            chk_interiors = new RadioButton();

            //
            // lbl_selectExportOption
            //
            lbl_selectExportOption.AutoSize = true;
            lbl_selectExportOption.ForeColor = Color.Gainsboro;
            lbl_selectExportOption.Location = new Point(3, 11);
            lbl_selectExportOption.Name = "lbl_selectExportOption";
            lbl_selectExportOption.Size = new Size(289, 13);
            lbl_selectExportOption.TabIndex = 3;
            lbl_selectExportOption.Text = "Select export option:";

            //
            // chk_Doors
            //
            chk_doors.AutoSize = true;
            chk_doors.FlatStyle = FlatStyle.Flat;
            chk_doors.ForeColor = Color.Gainsboro;
            chk_doors.Location = new Point(12, 41);
            chk_doors.Name = "chk_doors";
            chk_doors.Size = new Size(185, 17);
            chk_doors.TabIndex = 0;
            chk_doors.Text = "Doors";
            chk_doors.UseVisualStyleBackColor = true;
            //
            // chk_Markers
            //
            chk_markers.AutoSize = true;
            chk_markers.FlatStyle = FlatStyle.Flat;
            chk_markers.ForeColor = Color.Gainsboro;
            chk_markers.Location = new Point(12, 66);
            chk_markers.Name = "chk_markers";
            chk_markers.Size = new Size(185, 17);
            chk_markers.TabIndex = 0;
            chk_markers.Text = "Markers";
            chk_markers.UseVisualStyleBackColor = true;
            //
            // chk_Interiors
            //
            chk_interiors.AutoSize = true;
            chk_interiors.FlatStyle = FlatStyle.Flat;
            chk_interiors.ForeColor = Color.Gainsboro;
            chk_interiors.Location = new Point(12, 91);
            chk_interiors.Name = "chk_interiors";
            chk_interiors.Size = new Size(185, 17);
            chk_interiors.TabIndex = 0;
            chk_interiors.Text = "Interiors";
            chk_interiors.UseVisualStyleBackColor = true;

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(120, 55);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 2;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            btn_OK.Click += btn_OK_Click;

            chk_interiors.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_doors.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_markers.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);

            //Sets chk_Doors to be checked by default as it is the first option chronologically.
            chk_doors.Checked = true;

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            Text = "Map Info";
            Size = new Size(200, 160);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            ForeColor = Color.Gainsboro;
            MaximizeBox = false;
            MinimizeBox = false;

            Controls.Add(chk_doors);
            Controls.Add(chk_markers);
            Controls.Add(chk_interiors);

            Controls.Add(btn_OK);

            Controls.Add(lbl_selectExportOption);

            PerformLayout();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            StartProcess();
        }

        private void StartProcess()
        {
            if (File.Exists(@"CreationKit.exe"))
            {
                System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + parameterAddition);
                //this.Close(); - We don't automatically close this window as most use-cases require several generations.
            }
            else
            {
                using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                messageBox.ShowDialog();
            }
        }
    }
}
