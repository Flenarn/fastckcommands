﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FastCKCommands
{
    public partial class SaveDefaultPluginWindow: Form
    {

        //Buttons
        private Button btn_selectESM;
        private Button btn_OK;

        //Textboxes
        private TextBox txt_selectedESM;
        private TextBox txt_pluginName;

        //Labes
        private Label lbl_selectESMToMaster;
        private Label lbl_pluginName;

        //Booleans
        private bool esmSelected = false;

        //Strings
        public String parameters { get; set; }

        public SaveDefaultPluginWindow()
        {
            InitializeComponent();

            btn_selectESM = new Button();
            btn_OK = new Button();

            txt_selectedESM = new TextBox();
            txt_pluginName = new TextBox();

            lbl_selectESMToMaster = new Label();
            lbl_pluginName = new Label();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(130, 70);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 1;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;
            //
            // btn_selectESM
            //
            btn_selectESM.BackColor = Color.FromArgb(84, 84, 84);
            btn_selectESM.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_selectESM.FlatStyle = FlatStyle.Flat;
            btn_selectESM.Location = new Point(306, 33);
            btn_selectESM.Name = "btn_selectESM";
            btn_selectESM.Size = new Size(35, 22);
            btn_selectESM.TabIndex = 2;
            btn_selectESM.Text = "...";
            btn_selectESM.UseVisualStyleBackColor = false;

            //
            // txt_selectedESM
            //
            txt_selectedESM.BackColor = Color.FromArgb(84, 84, 84);
            txt_selectedESM.BorderStyle = BorderStyle.FixedSingle;
            txt_selectedESM.ForeColor = Color.Gainsboro;
            txt_selectedESM.Location = new Point(100, 33);
            txt_selectedESM.Name = "txt_selectedESM";
            txt_selectedESM.Size = new Size(199, 23);
            txt_selectedESM.TabIndex = 2;
            txt_selectedESM.PlaceholderText = "Select ESM...";
            txt_selectedESM.ReadOnly = true;
            //
            // txt_pluginName
            //
            txt_pluginName.BackColor = Color.FromArgb(84, 84, 84);
            txt_pluginName.BorderStyle = BorderStyle.FixedSingle;
            txt_pluginName.ForeColor = Color.Gainsboro;
            txt_pluginName.Location = new Point(100, 8);
            txt_pluginName.Name = "txt_pluginName";
            txt_pluginName.Size = new Size(199, 23);
            txt_pluginName.TabIndex = 2;
            txt_pluginName.PlaceholderText = "Enter a plugin name...";

            //
            // lbl_selectESMToProcess
            //
            lbl_selectESMToMaster.AutoSize = true;
            lbl_selectESMToMaster.ForeColor = Color.Gainsboro;
            lbl_selectESMToMaster.Location = new Point(3, 36);
            lbl_selectESMToMaster.Name = "lbl_selectESMToProcess";
            lbl_selectESMToMaster.Size = new Size(289, 13);
            lbl_selectESMToMaster.TabIndex = 3;
            lbl_selectESMToMaster.Text = "ESM to parent:";
            //
            // lbl_pluginName
            //
            lbl_pluginName.AutoSize = true;
            lbl_pluginName.ForeColor = Color.Gainsboro;
            lbl_pluginName.Location = new Point(3, 11);
            lbl_pluginName.Name = "lbl_pluginName";
            lbl_pluginName.Size = new Size(289, 13);
            lbl_pluginName.TabIndex = 3;
            lbl_pluginName.Text = "Plugin name:";

            Controls.Add(txt_selectedESM);
            Controls.Add(txt_pluginName);

            Controls.Add(btn_selectESM);
            Controls.Add(btn_OK);

            Controls.Add(lbl_selectESMToMaster);
            Controls.Add(lbl_pluginName);

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Size = new Size(362, 160);

            btn_selectESM.Click += btn_SelectESM_Click;
            btn_OK.Click += btn_OK_Click;

            PerformLayout();

        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            btn_OK_Process();
        }

        private void btn_OK_Process()
        {
            if (txt_pluginName.Text == "")
            {
                using MessageBox messageBox = new MessageBox("No plugin name!", "You must set a plugin name.");
                messageBox.ShowDialog();
            }
            else if (!esmSelected)
            {
                using MessageBox messageBox = new MessageBox("No ESM selected!", "You must select a ESM first.");
                messageBox.ShowDialog();
            }
            else
            {
                String tempName = txt_pluginName.Text.ToLower();

                if(tempName.EndsWith(".esp"))
                {
                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + txt_pluginName.Text + txt_selectedESM.Text);
                        this.Close();
                    }
                    else
                    {
                        using MessageBox messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                }
                else
                {
                    txt_pluginName.Text += ".esp";

                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters +":"+ txt_pluginName.Text + " " +  txt_selectedESM.Text);
                        this.Close();
                    }
                    else
                    {
                        using MessageBox messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                }
            }
        }
    

        //Calls file selection
        private void btn_SelectESM_Click(object sender, EventArgs e)
        {
            SelectESM();
        }

        private void SelectESM()
        {
            //File selection Properties (Only allow for ESMs for now)
            FileDialog fileDialogESM = new OpenFileDialog
            {
                DefaultExt = ".esm",
                Filter = "Elder Scrolls Master |*.esm"
            };
            _ = fileDialogESM.ShowDialog(this);

            txt_selectedESM.Text = Path.GetFileName(fileDialogESM.FileName);
            esmSelected = true;
        }
    }
}
