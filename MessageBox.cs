﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FastCKCommands
{
    internal partial class MessageBox : Form
    {
        //Labels
        private Label lbl_Message;

        //Buttons
        private Button btn_OK;

        //Strings
        public String message { get; set; }
        public String windowName { get; set; }

        public MessageBox(string title, string description)
        {
            InitializeComponent();

            lbl_Message = new Label();

            btn_OK = new Button();

            //
            // lbl_Message
            //
            lbl_Message.AutoSize = true;
            lbl_Message.ForeColor = Color.Gainsboro;
            lbl_Message.Location = new Point(50, 20);
            lbl_Message.Name = "lbl_Message";
            lbl_Message.Size = new Size(150, 150);
            lbl_Message.TabIndex = 3;
            lbl_Message.Text = description;


            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(120, 45);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 2;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            Text = title;
            Size = new Size(300, 120);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            ForeColor = Color.Gainsboro;
            MaximizeBox = false;
            MinimizeBox = false;

            Controls.Add(lbl_Message);
            Controls.Add(btn_OK);

            btn_OK.Click += btn_OK_Click;

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);

            this.Update();

            PerformLayout();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
