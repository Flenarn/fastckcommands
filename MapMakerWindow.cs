﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FastCKCommands
{
    public partial class MapMakerWindow : Form
    {

        //Buttons
        private Button btn_OK;

        //Textboxes
        private TextBox txt_AreaNameText;

        //Labes
        private Label lbl_areaNameText;

        //Strings
        public string parameters { get; set; }
        private String relativePath;

        public MapMakerWindow()
        {
            InitializeComponent();
            
            btn_OK = new Button();
            
            txt_AreaNameText = new TextBox();

            lbl_areaNameText = new Label();

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(130, 50);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 1;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            //
            // txt_AreaNameText
            //
            txt_AreaNameText.BackColor = Color.FromArgb(84, 84, 84);
            txt_AreaNameText.BorderStyle = BorderStyle.FixedSingle;
            txt_AreaNameText.ForeColor = Color.Gainsboro;
            txt_AreaNameText.Location = new Point(140, 8);
            txt_AreaNameText.Name = "txt_AreaText";
            txt_AreaNameText.Size = new Size(199, 23);
            txt_AreaNameText.TabIndex = 2;
            txt_AreaNameText.PlaceholderText = "Enter Area Name...";
           
            //
            // lbl_wavText
            //
            lbl_areaNameText.AutoSize = true;
            lbl_areaNameText.ForeColor = Color.Gainsboro;
            lbl_areaNameText.Location = new Point(3, 11);
            lbl_areaNameText.Name = "lbl_wavText";
            lbl_areaNameText.Size = new Size(289, 13);
            lbl_areaNameText.TabIndex = 3;
            lbl_areaNameText.Text = "Area to generate map:";

            Controls.Add(txt_AreaNameText);
            Controls.Add(btn_OK);
            Controls.Add(lbl_areaNameText);

            //BackColor, Font & size limitations
            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Size = new Size(362, 160);

            btn_OK.Click += btn_OK_Click;

            PerformLayout();

        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            btn_OK_Process();
        }

        private void btn_OK_Process()
        {
            if (txt_AreaNameText.Text == "")
            {
                using MessageBox messageBox = new MessageBox("No area text", relativePath);
                messageBox.ShowDialog();
            }
            else
            {
                if (File.Exists(@"CreationKit.exe"))
                {
                    System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + " " + txt_AreaNameText.Text);
                    this.Close();
                }
                else
                {
                    using MessageBox messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                    messageBox.ShowDialog();
                }
            }
        }
    }
}
