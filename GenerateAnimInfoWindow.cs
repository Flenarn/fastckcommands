﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FastCKCommands
{
    public partial class GenerateAnimInfoWindow : Form
    {
        //Buttons
        private Button btn_OK;

        //Checkboxes
        private RadioButton chk_MT;
        private RadioButton chk_Weapon;
        private RadioButton chk_Furniture;

        //Strings
        public string parameters { get; set; }
        private string parameterAddition;

        //Labels
        private Label lbl_selectExportOption;

        private void RadioButtonCheckedChanged(object sender, EventArgs e)
        {
            parameterAddition = "";
            if (chk_MT.Checked)
            {
                parameterAddition = " ..\\Data ..\\Data\\Meshes\\AnimTextData --speed";
            }
            if (chk_Weapon.Checked)
            {
                parameterAddition = " ..\\Data ..\\Data\\Meshes\\AnimTextData --speed --stance";
            }
            if (chk_Furniture.Checked)
            {
                parameterAddition = " ..\\Data ..\\Data\\Meshes\\AnimTextData";
            }
            
        }

        public GenerateAnimInfoWindow()
        {
            InitializeComponent();
            lbl_selectExportOption = new Label();

            btn_OK = new Button();

            chk_MT = new RadioButton();
            chk_Weapon = new RadioButton();
            chk_Furniture = new RadioButton();

            //
            // lbl_selectExportOption
            //
            lbl_selectExportOption.AutoSize = true;
            lbl_selectExportOption.ForeColor = Color.Gainsboro;
            lbl_selectExportOption.Location = new Point(3, 11);
            lbl_selectExportOption.Name = "lbl_selectExportOption";
            lbl_selectExportOption.Size = new Size(289, 13);
            lbl_selectExportOption.TabIndex = 3;
            lbl_selectExportOption.Text = "Select export option:";

            //
            // chk_MT
            //
            chk_MT.AutoSize = true;
            chk_MT.FlatStyle = FlatStyle.Flat;
            chk_MT.ForeColor = Color.Gainsboro;
            chk_MT.Location = new Point(12, 41);
            chk_MT.Name = "chk_MT";
            chk_MT.Size = new Size(185, 17);
            chk_MT.TabIndex = 0;
            chk_MT.Text = "Movement";
            chk_MT.UseVisualStyleBackColor = true;
            //
            // chk_Weapon
            //
            chk_Weapon.AutoSize = true;
            chk_Weapon.FlatStyle = FlatStyle.Flat;
            chk_Weapon.ForeColor = Color.Gainsboro;
            chk_Weapon.Location = new Point(12, 66);
            chk_Weapon.Name = "chk_Weapon";
            chk_Weapon.Size = new Size(185, 17);
            chk_Weapon.TabIndex = 0;
            chk_Weapon.Text = "Weapon";
            chk_Weapon.UseVisualStyleBackColor = true;
            //
            // chk_Furniture
            //
            chk_Furniture.AutoSize = true;
            chk_Furniture.FlatStyle = FlatStyle.Flat;
            chk_Furniture.ForeColor = Color.Gainsboro;
            chk_Furniture.Location = new Point(12, 91);
            chk_Furniture.Name = "chk_Furniture";
            chk_Furniture.Size = new Size(185, 17);
            chk_Furniture.TabIndex = 0;
            chk_Furniture.Text = "Furniture";
            chk_Furniture.UseVisualStyleBackColor = true;

            //
            // btn_OK
            //
            btn_OK.BackColor = Color.FromArgb(84, 84, 84);
            btn_OK.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_OK.FlatStyle = FlatStyle.Flat;
            btn_OK.Location = new Point(120, 55);
            btn_OK.Name = "btn_OK";
            btn_OK.Size = new Size(40, 22);
            btn_OK.TabIndex = 2;
            btn_OK.Text = "OK";
            btn_OK.UseVisualStyleBackColor = false;

            btn_OK.Click += btn_OK_Click;

            chk_Furniture.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_MT.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_Weapon.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);

            //Sets chk_MT to be checked by default as it is the first option chronologically.
            chk_MT.Checked = true;

            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            Text = "Generate Anim Info";
            Size = new Size(200, 160);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            ForeColor = Color.Gainsboro;
            MaximizeBox = false;
            MinimizeBox = false;

            Controls.Add(chk_MT);
            Controls.Add(chk_Weapon);
            Controls.Add(chk_Furniture);

            Controls.Add(btn_OK);

            Controls.Add(lbl_selectExportOption);

            PerformLayout();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            StartProcess();
        }

        private void StartProcess()
        {
            if (File.Exists(@"CreationKit.exe"))
            {
                System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + parameterAddition);
                //this.Close(); - We don't automatically close this window as most use-cases require several generations.
            }
            else
            {
                using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                messageBox.ShowDialog();
            }
        }
    }
}
