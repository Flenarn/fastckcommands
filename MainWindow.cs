﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FastCKCommands
{
    public partial class MainWindow : Form
    {
        //Checkboxes MainWindow (RadioButtons)
        private RadioButton chk_exportDismemberData;
        private RadioButton chk_convertMaterials;
        private RadioButton chk_generateLips;
        private RadioButton chk_buildCDX;
        private RadioButton chk_compressPSG;
        private RadioButton chk_exportFaceGenData;
        private RadioButton chk_generateStaticCollections;
        private RadioButton chk_exportText;
        private RadioButton chk_exportDialogue;
        private RadioButton chk_depresistRefs;
        private RadioButton chk_saveDefaultPlugin;
        private RadioButton chk_generateWarnings;
        private RadioButton chk_dumpNeededFiles;
        private RadioButton chk_convertToESL;
        private RadioButton chk_updateModelData;
        private RadioButton chk_generateSingleLip;
        private RadioButton chk_generateAnimInfo;
        private RadioButton chk_generatePrecombined;
        private RadioButton chk_generatePreVisData;
        private RadioButton chk_mapInfo;
        private RadioButton chk_mapMaker;

        //Buttons
        private Button btn_selectESM;
        private Button btn_startProcess;

        //Textboxes
        private TextBox txt_selectedESM;

        //Labes
        private Label lbl_selectESMToProcess;

        //Strings
        private String mainCommand;
        public String parameters { get; set; }

        //Booleans
        private bool esmSelected = false;
        private void RadioButtonCheckedChanged(object sender, EventArgs e) 
        {
            if(chk_compressPSG.Checked)
            {             
                mainCommand = chk_compressPSG.Text;
            }     

            if(chk_convertMaterials.Checked)
            {               
                mainCommand = chk_convertMaterials.Text;
            }

            if(chk_buildCDX.Checked)
            {              
                mainCommand = chk_buildCDX.Text;
            }

            if(chk_generateLips.Checked)
            {                
                mainCommand = chk_generateLips.Text;
            }

            if(chk_exportDismemberData.Checked)
            {                
                mainCommand = chk_exportDismemberData.Text;
            }

            if(chk_exportFaceGenData.Checked)
            {                
                mainCommand = chk_exportFaceGenData.Text;
            }

            if(chk_generateStaticCollections.Checked)
            {                
                mainCommand = chk_generateStaticCollections.Text;
            }

            if(chk_exportText.Checked)
            {
                mainCommand = chk_exportText.Text;
            }

            if(chk_exportDialogue.Checked)
            {
                mainCommand = chk_exportDialogue.Text;
            }

            if(chk_depresistRefs.Checked)
            {
                mainCommand = chk_depresistRefs.Text;
            }

            if(chk_saveDefaultPlugin.Checked)
            {
                mainCommand = chk_saveDefaultPlugin.Text;
            }

            if(chk_generateWarnings.Checked)
            {
                mainCommand = chk_generateWarnings.Text;
            }

            if(chk_dumpNeededFiles.Checked)
            {
                mainCommand = chk_dumpNeededFiles.Text;
            }

            if(chk_convertToESL.Checked)
            {
                mainCommand = chk_convertToESL.Text;
            }

            if(chk_updateModelData.Checked)
            {
                mainCommand = chk_updateModelData.Text;
            }

            if(chk_generateSingleLip.Checked)
            {
                mainCommand = chk_generateSingleLip.Text;
            }

            if(chk_generateAnimInfo.Checked)
            {
                mainCommand = chk_generateAnimInfo.Text;
            }

            if(chk_generatePrecombined.Checked)
            {
                mainCommand = chk_generatePrecombined.Text;
            }

            if(chk_generatePreVisData.Checked)
            {
                mainCommand = chk_generatePreVisData.Text;
            }

            if(chk_mapInfo.Checked)
            {
                mainCommand = chk_mapInfo.Text;
            }

            if(chk_mapMaker.Checked)
            {
                mainCommand = chk_mapMaker.Text;
            }
        }


        public MainWindow()
        {
            InitializeComponent();
 
            //Setup all the componnents
            chk_exportDismemberData = new RadioButton();
            chk_convertMaterials = new RadioButton();
            chk_generateLips = new RadioButton();
            chk_buildCDX = new RadioButton();
            chk_compressPSG = new RadioButton();
            chk_exportFaceGenData = new RadioButton();
            chk_generateStaticCollections = new RadioButton();
            chk_exportText = new RadioButton();
            chk_exportDialogue = new RadioButton();
            chk_depresistRefs = new RadioButton();
            chk_saveDefaultPlugin = new RadioButton();
            chk_generateWarnings = new RadioButton();
            chk_dumpNeededFiles = new RadioButton();
            chk_convertToESL = new RadioButton();
            chk_updateModelData = new RadioButton();
            chk_generateSingleLip = new RadioButton();
            chk_generateAnimInfo = new RadioButton();
            chk_generatePrecombined = new RadioButton();
            chk_generatePreVisData = new RadioButton();
            chk_mapInfo = new RadioButton();
            chk_mapMaker = new RadioButton();

            btn_selectESM = new Button();
            btn_startProcess = new Button();

            txt_selectedESM = new TextBox();

            lbl_selectESMToProcess = new Label();

            /// COMPONENT STYLING START ///

            //
            // chk_exportDismemberData
            //
            chk_exportDismemberData.AutoSize = true;
            chk_exportDismemberData.FlatStyle = FlatStyle.Flat;
            chk_exportDismemberData.ForeColor = Color.Gainsboro;
            chk_exportDismemberData.Location = new Point(12, 41);
            chk_exportDismemberData.Name = "chk_exportDismemberData";
            chk_exportDismemberData.Size = new Size(185, 17);
            chk_exportDismemberData.TabIndex = 0;
            chk_exportDismemberData.Text = "-ExportDismemberData";
            chk_exportDismemberData.UseVisualStyleBackColor = true;
            //
            // chk_convertMaterials
            //
            chk_convertMaterials.AutoSize = true;
            chk_convertMaterials.FlatStyle = FlatStyle.Flat;
            chk_convertMaterials.ForeColor = Color.Gainsboro;
            chk_convertMaterials.Location = new Point(12, 66);
            chk_convertMaterials.Name = "chk_convertMaterials";
            chk_convertMaterials.Size = new Size(185, 17);
            chk_convertMaterials.TabIndex = 0;
            chk_convertMaterials.Text = "-ConvertMaterials";
            chk_convertMaterials.UseVisualStyleBackColor = true;
            //
            // chk_generateLips
            //
            chk_generateLips.AutoSize = true;
            chk_generateLips.FlatStyle = FlatStyle.Flat;
            chk_generateLips.ForeColor = Color.Gainsboro;
            chk_generateLips.Location = new Point(12, 91);
            chk_generateLips.Name = "chk_generateLips";
            chk_generateLips.Size = new Size(185, 17);
            chk_generateLips.TabIndex = 0;
            chk_generateLips.Text = "-GenerateLips";
            chk_generateLips.UseVisualStyleBackColor = true;
            //
            // chk_buildCDX
            //
            chk_buildCDX.AutoSize = true;
            chk_buildCDX.FlatStyle = FlatStyle.Flat;
            chk_buildCDX.ForeColor = Color.Gainsboro;
            chk_buildCDX.Location = new Point(12, 116);
            chk_buildCDX.Name = "chk_buildCDX";
            chk_buildCDX.Size = new Size(185, 17);
            chk_buildCDX.TabIndex = 0;
            chk_buildCDX.Text = "-BuildCDX";
            chk_buildCDX.UseVisualStyleBackColor = true;
            //
            // chk_compressPSG
            //
            chk_compressPSG.AutoSize = true;
            chk_compressPSG.FlatStyle = FlatStyle.Flat;
            chk_compressPSG.ForeColor = Color.Gainsboro;
            chk_compressPSG.Location = new Point(12, 141);
            chk_compressPSG.Name = "chk_compressPSG";
            chk_compressPSG.Size = new Size(185, 17);
            chk_compressPSG.TabIndex = 0;
            chk_compressPSG.Text = "-CompressPSG";
            chk_compressPSG.UseVisualStyleBackColor = true;
            //
            // chk_exportFaceGenData
            //
            chk_exportFaceGenData.AutoSize = true;
            chk_exportFaceGenData.FlatStyle = FlatStyle.Flat;
            chk_exportFaceGenData.ForeColor = Color.Gainsboro;
            chk_exportFaceGenData.Location = new Point(12, 166);
            chk_exportFaceGenData.Name = "chk_exportFaceGenData";
            chk_exportFaceGenData.Size = new Size(185, 17);
            chk_exportFaceGenData.TabIndex = 0;
            chk_exportFaceGenData.Text = "-ExportFaceGenData";
            chk_exportFaceGenData.UseVisualStyleBackColor = true;
            //
            // chk_generateStaticCollections
            //
            chk_generateStaticCollections.AutoSize = true;
            chk_generateStaticCollections.FlatStyle = FlatStyle.Flat;
            chk_generateStaticCollections.ForeColor = Color.Gainsboro;
            chk_generateStaticCollections.Location = new Point(12, 191);
            chk_generateStaticCollections.Name = "chk_generateStaticCollections";
            chk_generateStaticCollections.Size = new Size(185, 17);
            chk_generateStaticCollections.TabIndex = 0;
            chk_generateStaticCollections.Text = "-GenerateStaticCollections";
            chk_generateStaticCollections.UseVisualStyleBackColor = true;
            //
            // chk_exportText
            //
            chk_exportText.AutoSize = true;
            chk_exportText.FlatStyle = FlatStyle.Flat;
            chk_exportText.ForeColor = Color.Gainsboro;
            chk_exportText.Location = new Point(12, 216);
            chk_exportText.Name = "chk_exportText";
            chk_exportText.Size = new Size(185, 17);
            chk_exportText.TabIndex = 0;
            chk_exportText.Text = "-ExportText";
            chk_exportText.UseVisualStyleBackColor = true;
            //
            // chk_exportDialogue
            //
            chk_exportDialogue.AutoSize = true;
            chk_exportDialogue.FlatStyle = FlatStyle.Flat;
            chk_exportDialogue.ForeColor = Color.Gainsboro;
            chk_exportDialogue.Location = new Point(12, 241);
            chk_exportDialogue.Name = "chk_exportDialogue";
            chk_exportDialogue.Size = new Size(185, 17);
            chk_exportDialogue.TabIndex = 0;
            chk_exportDialogue.Text = "-ExportDialogue";
            chk_exportDialogue.UseVisualStyleBackColor = true;
            //
            // chk_depresistRefs
            //
            chk_depresistRefs.AutoSize = true;
            chk_depresistRefs.FlatStyle = FlatStyle.Flat;
            chk_depresistRefs.ForeColor = Color.Gainsboro;
            chk_depresistRefs.Location = new Point(12, 266);
            chk_depresistRefs.Name = "chk_depresistRefs";
            chk_depresistRefs.Size = new Size(185, 17);
            chk_depresistRefs.TabIndex = 0;
            chk_depresistRefs.Text = "-DepersistRefs";
            chk_depresistRefs.UseVisualStyleBackColor = true;
            //
            // chk_saveDefaultPlugin
            //
            chk_saveDefaultPlugin.AutoSize = true;
            chk_saveDefaultPlugin.FlatStyle = FlatStyle.Flat;
            chk_saveDefaultPlugin.ForeColor = Color.Gainsboro;
            chk_saveDefaultPlugin.Location = new Point(12, 291);
            chk_saveDefaultPlugin.Name = "chk_saveDefaultPlugin";
            chk_saveDefaultPlugin.Size = new Size(185, 17);
            chk_saveDefaultPlugin.TabIndex = 0;
            chk_saveDefaultPlugin.Text = "-SaveDefaultPlugin";
            chk_saveDefaultPlugin.UseVisualStyleBackColor = true;
            //
            // chk_generateWarnings
            //
            chk_generateWarnings.AutoSize = true;
            chk_generateWarnings.FlatStyle = FlatStyle.Flat;
            chk_generateWarnings.ForeColor = Color.Gainsboro;
            chk_generateWarnings.Location = new Point(12, 316);
            chk_generateWarnings.Name = "chk_generateWarnings";
            chk_generateWarnings.Size = new Size(185, 17);
            chk_generateWarnings.TabIndex = 0;
            chk_generateWarnings.Text = "-GenerateWarnings";
            chk_generateWarnings.UseVisualStyleBackColor = true;
            //
            // chk_dumpNeededFiles
            //
            chk_dumpNeededFiles.AutoSize = true;
            chk_dumpNeededFiles.FlatStyle = FlatStyle.Flat;
            chk_dumpNeededFiles.ForeColor = Color.Gainsboro;
            chk_dumpNeededFiles.Location = new Point(12, 341);
            chk_dumpNeededFiles.Name = "chk_dumpNeededFiles";
            chk_dumpNeededFiles.Size = new Size(185, 17);
            chk_dumpNeededFiles.TabIndex = 0;
            chk_dumpNeededFiles.Text = "-DumpNeededFiles";
            chk_dumpNeededFiles.UseVisualStyleBackColor = true;
            //
            // chk_convertToESL
            //
            chk_convertToESL.AutoSize = true;
            chk_convertToESL.FlatStyle = FlatStyle.Flat;
            chk_convertToESL.ForeColor = Color.Gainsboro;
            chk_convertToESL.Location = new Point(252, 41);
            chk_convertToESL.Name = "chk_convertToESL";
            chk_convertToESL.Size = new Size(185, 17);
            chk_convertToESL.TabIndex = 0;
            chk_convertToESL.Text = "-ConvertToESL";
            chk_convertToESL.UseVisualStyleBackColor = true;
            //
            // chk_updateModelData
            //
            chk_updateModelData.AutoSize = true;
            chk_updateModelData.FlatStyle = FlatStyle.Flat;
            chk_updateModelData.ForeColor = Color.Gainsboro;
            chk_updateModelData.Location = new Point(252, 66);
            chk_updateModelData.Name = "chk_updateModelData";
            chk_updateModelData.Size = new Size(185, 17);
            chk_updateModelData.TabIndex = 0;
            chk_updateModelData.Text = "-UpdateModelData";
            chk_updateModelData.UseVisualStyleBackColor = true;
            //
            // chk_generateSingleLip
            //
            chk_generateSingleLip.AutoSize = true;
            chk_generateSingleLip.FlatStyle = FlatStyle.Flat;
            chk_generateSingleLip.ForeColor = Color.Gainsboro;
            chk_generateSingleLip.Location = new Point(252, 91);
            chk_generateSingleLip.Name = "chk_generateSingleLip";
            chk_generateSingleLip.Size = new Size(185, 17);
            chk_generateSingleLip.TabIndex = 0;
            chk_generateSingleLip.Text = "-GenerateSingleLip";
            chk_generateSingleLip.UseVisualStyleBackColor = true;
            //
            // chk_generateAnimInfo
            //
            chk_generateAnimInfo.AutoSize = true;
            chk_generateAnimInfo.FlatStyle = FlatStyle.Flat;
            chk_generateAnimInfo.ForeColor = Color.Gainsboro;
            chk_generateAnimInfo.Location = new Point(252, 116);
            chk_generateAnimInfo.Name = "chk_generateAnimInfo";
            chk_generateAnimInfo.Size = new Size(185, 17);
            chk_generateAnimInfo.TabIndex = 0;
            chk_generateAnimInfo.Text = "-GenerateAnimInfo";
            chk_generateAnimInfo.UseVisualStyleBackColor = true;
            //
            // chk_generatePreCombined
            //
            chk_generatePrecombined.AutoSize = true;
            chk_generatePrecombined.FlatStyle = FlatStyle.Flat;
            chk_generatePrecombined.ForeColor = Color.Gainsboro;
            chk_generatePrecombined.Location = new Point(252, 141);
            chk_generatePrecombined.Name = "chk_generatePrecombined";
            chk_generatePrecombined.Size = new Size(185, 17);
            chk_generatePrecombined.TabIndex = 0;
            chk_generatePrecombined.Text = "-GeneratePrecombined";
            chk_generatePrecombined.UseVisualStyleBackColor = true;
            //
            // chk_generatePreVisData
            //
            chk_generatePreVisData.AutoSize = true;
            chk_generatePreVisData.FlatStyle = FlatStyle.Flat;
            chk_generatePreVisData.ForeColor = Color.Gainsboro;
            chk_generatePreVisData.Location = new Point(252, 166);
            chk_generatePreVisData.Name = "chk_generatePreVisData";
            chk_generatePreVisData.Size = new Size(185, 17);
            chk_generatePreVisData.TabIndex = 0;
            chk_generatePreVisData.Text = "-GeneratePreVisData";
            chk_generatePreVisData.UseVisualStyleBackColor = true;
            //
            // chk_mapInfo
            //
            chk_mapInfo.AutoSize = true;
            chk_mapInfo.FlatStyle = FlatStyle.Flat;
            chk_mapInfo.ForeColor = Color.Gainsboro;
            chk_mapInfo.Location = new Point(252, 191);
            chk_mapInfo.Name = "chk_mapInfo";
            chk_mapInfo.Size = new Size(185, 17);
            chk_mapInfo.TabIndex = 0;
            chk_mapInfo.Text = "-MapInfo";
            chk_mapInfo.UseVisualStyleBackColor = true;
            //
            // chk_mapMaker
            //
            chk_mapMaker.AutoSize = true;
            chk_mapMaker.FlatStyle = FlatStyle.Flat;
            chk_mapMaker.ForeColor = Color.Gainsboro;
            chk_mapMaker.Location = new Point(252, 216);
            chk_mapMaker.Name = "chk_mapMaker";
            chk_mapMaker.Size = new Size(185, 17);
            chk_mapMaker.TabIndex = 0;
            chk_mapMaker.Text = "-MapMaker";
            chk_mapMaker.UseVisualStyleBackColor = true;

            //
            // btn_selectESM
            //
            btn_selectESM.BackColor = Color.FromArgb(84, 84, 84);
            btn_selectESM.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_selectESM.FlatStyle = FlatStyle.Flat;
            btn_selectESM.Location = new Point(306, 8);
            btn_selectESM.Name = "btn_selectESM";
            btn_selectESM.Size = new Size(35, 22);
            btn_selectESM.TabIndex = 2;
            btn_selectESM.Text = "...";
            btn_selectESM.UseVisualStyleBackColor = false;
            //
            // btn_startProcess
            //
            btn_selectESM.BackColor = Color.FromArgb(84, 84, 84);
            btn_selectESM.FlatAppearance.BorderColor = Color.FromArgb(114, 114, 114);
            btn_startProcess.FlatStyle = FlatStyle.Flat;
            btn_startProcess.Location = new Point(549, 360);
            btn_startProcess.Name = "btn_startProcess";
            btn_startProcess.Size = new Size(125, 23);
            btn_startProcess.TabIndex = 2;
            btn_startProcess.Text = "Start Process...";
            btn_startProcess.UseVisualStyleBackColor = false;

            //
            // txt_selectedESM
            //
            txt_selectedESM.BackColor = Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(84)))));
            txt_selectedESM.BorderStyle = BorderStyle.FixedSingle;
            txt_selectedESM.ForeColor = Color.Gainsboro;
            txt_selectedESM.Location = new Point(100, 8);
            txt_selectedESM.Name = "txt_selectedESM";
            txt_selectedESM.Size = new Size(199, 23);
            txt_selectedESM.TabIndex = 3;
            txt_selectedESM.PlaceholderText = "Select ESM...";
            txt_selectedESM.ReadOnly = true;

            //
            // lbl_selectESMToProcess
            //
            lbl_selectESMToProcess.AutoSize = true;
            lbl_selectESMToProcess.ForeColor = Color.Gainsboro;
            lbl_selectESMToProcess.Location = new Point(3, 11);
            lbl_selectESMToProcess.Name = "lbl_selectESMToProcess";
            lbl_selectESMToProcess.Size = new Size(289, 13);
            lbl_selectESMToProcess.TabIndex = 3;
            lbl_selectESMToProcess.Text = "ESM to process:";

            /// COMPONENT STYLING END ///

            //Add all elements to the controls to ensure they render
            Controls.Add(chk_exportDismemberData);
            Controls.Add(chk_convertMaterials);
            Controls.Add(chk_generateLips);
            Controls.Add(chk_buildCDX);
            Controls.Add(chk_compressPSG);
            Controls.Add(chk_exportFaceGenData);
            Controls.Add(chk_generateStaticCollections);
            Controls.Add(chk_exportText);
            Controls.Add(chk_exportDialogue);
            Controls.Add(chk_depresistRefs);
            Controls.Add(chk_saveDefaultPlugin);
            Controls.Add(chk_generateWarnings);
            Controls.Add(chk_dumpNeededFiles);
            Controls.Add(chk_convertToESL);
            Controls.Add(chk_updateModelData);
            Controls.Add(chk_generateSingleLip);
            Controls.Add(chk_generateAnimInfo);
            Controls.Add(chk_generatePrecombined);
            Controls.Add(chk_generatePreVisData);
            Controls.Add(chk_mapInfo);
            Controls.Add(chk_mapMaker);

            Controls.Add(btn_startProcess);
            Controls.Add(btn_selectESM);

            Controls.Add(txt_selectedESM);

            Controls.Add(lbl_selectESMToProcess);

            btn_selectESM.Click += btn_SelectESM_Click;
            btn_startProcess.Click += btn_StartProcess_Click;

            //Add the RadioButton CheckedChanged event to all options
            chk_convertMaterials.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_buildCDX.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_compressPSG.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generateLips.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_exportDismemberData.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_exportFaceGenData.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generateStaticCollections.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_exportText.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_exportDialogue.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_depresistRefs.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_saveDefaultPlugin.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generateWarnings.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_dumpNeededFiles.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_convertToESL.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_updateModelData.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generateSingleLip.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generateAnimInfo.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generatePrecombined.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_generatePreVisData.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_mapInfo.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);
            chk_mapMaker.CheckedChanged += new EventHandler(RadioButtonCheckedChanged);

            //Sets chk_exportDismemberData to be checked by default as it is the first option chronologically.
            chk_exportDismemberData.Checked = true;

            //BackColor, Font & size limitations
            BackColor = Color.FromArgb(50, 50, 50);
            Font = new Font("Consolas", 8F);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;

            PerformLayout();
        }

        //Calls file selection
        private void btn_SelectESM_Click(object sender, EventArgs e)
        {
            SelectESM();
        }


        private void SelectESM()
        {
            //File selection Properties (Only allow for ESMs for now)
            FileDialog fileDialogESM = new OpenFileDialog
            {
                DefaultExt = ".esm",
                Filter = "Elder Scrolls Master |*.esm"
            };
            _ = fileDialogESM.ShowDialog(this);

            txt_selectedESM.Text = Path.GetFileName(fileDialogESM.FileName);
            esmSelected = true;
        }

        private void btn_StartProcess_Click(object sender, EventArgs e)
        {
            if(!esmSelected && !chk_convertMaterials.Checked && !chk_saveDefaultPlugin.Checked && !chk_convertToESL.Checked && !chk_generateSingleLip.Checked && txt_selectedESM.Text == "")
            {
                using MessageBox messageBox = new MessageBox("No ESM selected!", "You must select a ESM first.");
                messageBox.ShowDialog();
            } 
            else 
            {

                //Convert materials doesn't require ESM selection, so we trim it out of the commandline
                parameters = chk_convertMaterials.Checked || chk_convertToESL.Checked || chk_saveDefaultPlugin.Checked  || chk_generateSingleLip.Checked ? " " + mainCommand : " " + mainCommand + (":") + txt_selectedESM.Text;

                //Options that require input on what platform to generate the data for

                // TODO - in cojunction with CK Fixes, add platform support for precomb/previs.
                if (chk_exportDismemberData.Checked || chk_exportFaceGenData.Checked | chk_generateStaticCollections.Checked)
                {
                    var platformWindow = new PlatformWindow
                    {
                        parameters = this.parameters
                    };
                    platformWindow.ShowDialog();
                }

                //Save a Default Plugin
                else if (chk_saveDefaultPlugin.Checked)
                {
                    var defaultPluginWindow = new SaveDefaultPluginWindow
                    {
                        parameters = this.parameters
                    };
                    defaultPluginWindow.ShowDialog();
                }

                //Generate Anim Info
                else if (chk_generateAnimInfo.Checked)
                {
                    var generateAnimInfoWindow = new GenerateAnimInfoWindow
                    {
                        parameters = this.parameters
                    };
                    generateAnimInfoWindow.ShowDialog();
                }

                //Map Info
                else if(chk_mapInfo.Checked)
                {
                    var mapInfoWindow = new MapInfoWindow
                    {
                        parameters = this.parameters
                    };
                    mapInfoWindow.ShowDialog();
                }

                //If lip-sync generation, start the 32-bit Creation Kit
                else if (chk_generateLips.Checked)
                {
                    if (File.Exists(@"CreationKit32.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit32.exe", parameters);
                        this.Close();
                    }
                    else
                    {
                        using MessageBox messageBox = new MessageBox("Could not start CreationKit32.exe", "Check that the file exists!");
                        messageBox.ShowDialog();

                    }
                }

                //If option requires a .txt file to output content to
                else if (chk_dumpNeededFiles.Checked)
                {
                    var txtNameWindow = new txtNameWindow
                    {
                        parameters = this.parameters
                    };
                    txtNameWindow.ShowDialog();
                }

                //Generate Single Lips
                else if (chk_generateSingleLip.Checked)
                {
                    var generateSingleLipWindow = new GenerateSingleLipWindow
                    {
                        parameters = this.parameters
                    };
                    generateSingleLipWindow.ShowDialog();
                }

                //Map Maker
                else if (chk_mapMaker.Checked)
                {
                    var mapMakerWindow = new MapMakerWindow
                    {
                        parameters = this.parameters
                    };
                    mapMakerWindow.ShowDialog();
                }

                //ESP Selection for ESL conversion
                else if (chk_convertToESL.Checked)
                {
                    var espSelectionWindow = new ESPSelectionWindow
                    {
                        parameters = this.parameters
                    };
                    espSelectionWindow.ShowDialog();
                }

                //PreCombine/PreVis Generation
                else if(chk_generatePrecombined.Checked || chk_generatePreVisData.Checked)
                {
                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters + " clean all");
                    }
                    else
                    {
                        using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                }

                //No extra input needed, no custom command addition, the most basic of the options lead here simply.
                else
                {
                    if (File.Exists(@"CreationKit.exe"))
                    {
                        System.Diagnostics.Process.Start(@"CreationKit.exe", parameters);
                    }
                    else
                    {
                        using var messageBox = new MessageBox("Could not start CreationKit.exe", "Check that the file exists!");
                        messageBox.ShowDialog();
                    }
                }
            }
        }
    }
}
